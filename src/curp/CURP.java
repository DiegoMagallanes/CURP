package curp;

/**
 * En esta clase deben de terminar de implementar los metodos,
 * pueden definir metodos auxiliares si lo consideran necesario.
 * No deben de modificar la firma de ningun metodo.
 * @author diego
 */
public class CURP {
    /**
     * Metodo que regresa el primer caracter del nombre (Con el formato adecuado).
     * Deben considerar que el curp no lleva acentos.
     * @param nombre Nombre(s) del usuario.
     * @return El primer caracter del nombre.
     */
    String nombre(String nombre){
        return "";
    }
    
    /**
     * Metodo que regresa los primeros dos caracteres del apellido paterno (Con el formato adecuado).
     * Deben considerar que el curp no lleva acentos.
     * @param apellido_p El apellido paterno del usuario.
     * @return Los dos primero caracteres del apellido paterno.
     */
    String apellidoPaterno(String apellido_p){
        return "";
    }
    
    /**
     * Metodo que regresa el primer caracter del apellido materno (Con el formato adecuado).
     * Deben considerar que el curp no lleva acentos.
     * @param apellido_m El apellido materno del usuario.
     * @return El primer caracter del apellido materno.
     */
    String apellidoMaterno(String apellido_m){
        return "";
    }
    
    /**
     * Metodo que regresa el mes en formato: mm.
     * El metodo debe de aceptar el nombre del mes sin importar si vienen en minusculas o
     * mayusculas o mixto, tambien debe de aceptar si el mes fue dado como un entero.
     * Ejemplos de posibles entradas:
     * - Enero
     * - enero
     * - EnErO
     * - 01
     * - 1
     * Para cualquiera de las entradas anteriores la cadena que debe regresar es: 01
     * @param mes El mes dado.
     * @return El mes en formato: mm.
     */
    String getMes(String mes){
        return "";
    }
    
    /**
     * Metodo que regresa la fecha de nacimiento en el formato correcto: aammdd
     * La fecha viene en el siguiente formato: dd/mm/aaaa
     * Hint: Usen el metodo split para obtener los datos.
     * @param fecha La fecha de nacimiento del usuario.
     * @return La fecha en formato: aammdd.
     */
    String fechaNacimiento(String fecha){
        return "";
    }
    
    /**
     * Metodo que regresa el primer caracter del sexo dado, en el formato correcto.
     * @param sexo El sexo del usuario.
     * @return El primer caracter del sexo.
     */
    String sexo(String sexo){
        return "";
    }
    
    /**
     * Metodo que regresa la abreviatura del estado, en el formato correcto.
     * El metodo debe de aceptar entradas en minusculas, mayusculas o mixta,
     * tambien debe de aceptar entradas con acentos y sin acentos.
     * @param estado El estado de nacimiento del usuario.
     * @return La abreviatura del usuario.
     */
    String estado(String estado){
        return "";
    }
    
    /**
     * Metodo que regresa la segunda consonante de una cadena.
     * Hint: remplacen las vocales con acentos por vocales sin acentos y
     * pasen toda la cadena a minusculas o mayusculas para que tengan menos casos que revisar.
     * @param cadena La cadena dada.
     * @return La segunda consonante de una cadena.
     */
    char getConsonantes(String cadena){
        return ' ';
    }
}